// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 引入 axios
import axios from "axios";

// 配置基础路径https:
axios.defaults.baseURL = "https://www.fastmock.site/mock/c61ccf094071ddf68f90903571598a57/pethome";
//全局属性配置，在任意组件内可以使用this.$http获取axios对象、
Vue.prototype.$http = axios;



Vue.use(ElementUI);

// 引入本地mock
// import MockUser from './js/user/user.js'
import MockUser from './js/user/user.js'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
